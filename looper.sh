I=0
II=0
NEXTPID=0

if [[ $1 -ne 0 ]]; then
  echo $1 more to go
  ./looper.sh $(($1 - 1)) 1 &
  NEXTPID=$!
fi

while [[ $II -lt 11 ]]; do
  I=$(($I + 1))
  if [[ $I -eq 99999 ]]; then
    I=0
    II=$(($II + 1))
    echo $II
    if [[ $II -eq 10 ]]; then
      echo $1 done
      II=12
    fi
  fi
done

if [[ $1 -ne 0 ]]; then
  wait $NEXTPID
fi
