if [[ $1 -ge 0 ]]; then

  Time_Init=`date`

  echo ---- STARTING SPEEDFAN ----
  ./sf/speedfan.exe &    # Speedfan executable
  SFPID=$!
  echo ---- SPEEDFAN STARTED ----
  sleep 12

  Time_Log_Start=`date`

  echo ---- WAITING 60 SECONDS ----
  sleep 60

  Time_Loops_Start=`date`

  ./looper.sh $1
  echo ---- LOOPS FINISHED ----

  Time_Loops_End=`date`

  if [[ $2 -ne 1 ]]; then
    echo ---- BEGINNING 10 MINUTE SLEEP ----
    sleep 120
    echo ---- 8 MINUTES LEFT ----
    sleep 120
    echo ---- 6 MINUTES LEFT ----
    sleep 120
    echo ---- 4 MINUTES LEFT ----
    sleep 120
    echo ---- 2 MINUTES LEFT ----
    sleep 120
    echo ---- SLEEP DONE: ENDING ----
  fi

  kill $SFPID

  Time_Log_End=`date`

  echo Run finished at $Time_Log_End with $1 iterations.

  echo -e \\n---- RUN `date` with $1 iterations ----\\nTime Init: $Time_Init\\nTime Log Start: $Time_Log_Start\\nTime Loops Start: $Time_Loops_Start\\nTime Loops End: $Time_Loops_End\\nTime Log End: $Time_Log_End >> timeLog.txt

else
  echo "Please supply a valid argument (integer greater than or equal to 0!)"
  exit
fi
